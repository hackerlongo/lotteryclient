/**
 * !#zh 11选5走势图走势组件 
 * @information 走势 
 */
cc.Class({
    extends: cc.Component,

    properties: {
        labNums:{
            default:[],
            type:cc.Label
        },

        labIssue:{
            default:null,
            type:cc.Label
        },

        _data:null
    },

    // use this for initialization
    onLoad: function () {
        if(this._data != null)
        {
            this.node.color = this._data.color;
            var color = new cc.Color(255, 255, 255);
            var isus =  this._data.Isuse.toString();
            var isuseStr = isus.substring(isus.length-2,isus.length) + "期";
            this.labIssue.string = isuseStr;
            for(var i=0;i<this._data.nums.length;i++)
            {
                if(this._data.nums[i] != 0)
                {
                    this.labNums[i].string = this._data.nums[i].toString();
                }
                else
                {
                    var num = i+1;
                    this.labNums[i].string = num>9?num:"0"+num;
                    this.labNums[i].node.color = color;
                    this.labNums[i].node.parent.getChildByName("spBg").active = true;
                }
            }
        }
    },

    /** 
    * 接收走势图走势信息
    * @method init
    * @param {Object} data
    */
    init:function(data){
        this._data = data;
    }

});
