cc.Class({
    extends: cc.Component,

    properties: {
        spriteList:{
            default:[],
            type:cc.Sprite
        },
        
        labIsuse:{
            default:null,
            type:cc.Label
        },

        labState :{
            default:null,
            type:cc.Label
        }
    },

    // use this for initialization
    onLoad: function () {

    },

    init:function(data){
        for(var i=0;i<data.nums.length;i++)
        {
            try {
                this.spriteList[i].spriteFrame = data.nums[i];
            } catch (error) {
                cc.log(error);
            }
            
        }
       this.labIsuse.string = data.isuse;
       this.labState.string = data.state;
    },
});
