/**
 * !#zh 彩种组件
 * @information 彩种类型，最高奖
 */
cc.Class({
    extends: cc.Component,

    properties: {
        spLotteryIcon:{
            default: null,
            type: cc.Sprite
        },

        spLotteryState:{
            default: null,
            type: cc.Sprite
        },

        labLotteryName:{
            default: null,
            type: cc.Label
        },

        labLotteryDesc:{
            default: null,
            type: cc.Label
        },

        labLotteryRedDesc:{
            default: null,
            type: cc.Label
        }
    },

    // use this for initialization
    /** 
    * 彩种信息数据
    * @method init
    * @param {Object} data
    */
    init: function (data) {
        this.spLotteryIcon.spriteFrame = data.LotteryIconFrame;
        this.spLotteryState.spriteFrame = data.LotteryStatusFrame;
        this.labLotteryName.string = data.LotteryName;
        this.labLotteryDesc.string = data.Subhead || ""; 
        this.labLotteryRedDesc.string = data.AddawardSubhead || "";    
    },

    setState:function(state){
        this.spLotteryState.spriteFrame = state;
    }

});
