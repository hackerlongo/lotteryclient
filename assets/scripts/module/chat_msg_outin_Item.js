/**
 * !#zh 聊天组件
 * @information 自己输入的聊天信息
 */
cc.Class({
    extends: cc.Component,

    properties: {
        spHead:{
            default: null,
            type: cc.Sprite
        },
        spChatMsgBg:{
            default: null,
            type: cc.Node
        },
        labName:{
            default: null,
            type: cc.Label
        },

        rtTxt:{
            default: null,
            type: cc.RichText
        },

        spHeadBase:{
            default: null,
            type: cc.SpriteFrame
        },

        _data:null
    },

    // use this for initialization
    onLoad: function () {
        if(this._data != null)
        {
            if(this._data.head != "" && typeof(this._data.head) != "undefined"){
                cc.loader.load(this._data.head, function(err, tex){
                    if(tex != null){
                        var sf = new cc.SpriteFrame(tex);
                        this.spHead.spriteFrame = sf;
                        this.spHead.node.width = 127;
                        this.spHead.node.height = 127;
                    }
                    else
                    {
                        this.spHead.spriteFrame = this.spHeadBase;
                    }
                }.bind(this));
            } 
            else
            {
                this.spHead.spriteFrame = this.spHeadBase;
            }
            this.labName.string = this._data.name;

            var txt = this.analysis(this._data.txt);
            this.rtTxt.string = txt;
            var width = this.rtTxt._linesWidth.length>1 ? 700:this.rtTxt._linesWidth[0];
            var height = this.rtTxt._labelHeight;
            this.spChatMsgBg.width += width+20;
            this.spChatMsgBg.height = height+50;
 
            if(this.rtTxt._linesWidth.length > 1)
            {
                this.rtTxt.horizontalAlign = cc.TextAlignment.LEFT;
            }
            this.node.height =  this.spChatMsgBg.height + 50;
        }

    },

    /** 
    * 接收聊天信息数据
    * @method init
    * @param {Object} data
    */
    init: function(data){
       this._data = data;
    },

    /** 
    * 对聊天数据信息处理
    * @method analysis
    * @param {String} content
    * @return {String} 
    */
    analysis:function(content){
        var textHead = "<color=#000000>";
        var textFoot = "</color>";
        var imageHead = "<img src=";//<size=80></size>
        var imageFoot = "/>";

        if(content == ""){
            return textHead+textFoot;
        }

        var richTextStr = textHead;
        
        //判断是否存在"[",直到遍历到"["或者“]”为止，当遍历到“[”，并且遍历到"]",判断当中的标志是否表情标志
        for(var i = 0; i<content.length;){
            var str = content.charAt(i);
            
            if(str == "["){//判断是否是表情的开头

                var emotionStr = "[";
                var cacheStr = "[";
                var isEmotionStr = false;
                //检索当前以下所有，是否有表情的结尾
                for(var j = i+1; j<content.length; j++){
                    var currentWord = content.charAt(j);
                    emotionStr = emotionStr+currentWord;
                    cacheStr = cacheStr+currentWord;
                    if(currentWord == "["){
                        i = j
                        break;
                    }else if(currentWord == "]"){
                        isEmotionStr = this.getIsEmotionStr(emotionStr);
                        if(isEmotionStr == true){
                            //强制设置表情字符串为一个中文字符串长度，并且强制设置字符串
                            var emotionContentStr = this.getEmotionStrByContent(emotionStr);
                            richTextStr = richTextStr+imageHead+emotionContentStr+imageFoot;
                        }
                        i = j;
                        break;
                    }
   
                }
                if(isEmotionStr == false){
                    richTextStr = richTextStr+cacheStr;
                }
                 }else{
                     richTextStr = richTextStr+str;
                 }
                 i = i+1;
        }
        
        richTextStr = richTextStr+textFoot;
       
        return richTextStr;
    },

    getIsEmotionStr:function(str){
       //判断是否是表情的字符串
       var emotionArray = [
           "[1]",
           "[2]",
           "[3]",
           "[4]",
           "[5]",
           "[6]",
           "[7]",
           "[8]",
           "[9]",
           "[10]",
           "[11]",
           "[12]",
           "[13]",
           "[14]",
           "[15]",
           "[16]",
           "[17]",
           "[18]",
           "[19]",
           "[20]",
           "[21]",
           "[22]",
           "[23]",
           "[24]",
           "[25]",
           "[26]",
           "[27]",
           "[28]",
           "[29]",
           "[30]"
           ];
        var isEmotionStr = false;
        for(var i = 0; i<emotionArray.length; i++){
            if(str == emotionArray[i]){
                isEmotionStr = true;
                return isEmotionStr;
            }
        }
        return isEmotionStr;

    },

    getEmotionStrByContent:function(content){
        var contentStr = content.substring(1,content.length-1);
        return "\""+contentStr+"\"";
    },

    getHeight:function(){
        return this.node.height;
    }

});
