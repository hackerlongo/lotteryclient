cc.Class({
    extends: cc.Component,

    properties: {
        _data:null,
        webview:{
            default:null,
            type:cc.WebView
        }
    },

    // use this for initialization
    onLoad: function () {
        if(this._data != null)
        {
           this.webview.url = this._data;
        }
    },

    init:function(data){
        this._data = data;
    },

    onClose:function(){
        this.node.getComponent("Page").backAndRemove();
    },
    
});
