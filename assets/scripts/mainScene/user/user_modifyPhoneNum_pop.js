/**
 * !#zh 更换手机号码组件
 * @information 当前手机号,验证码
 */
cc.Class({
    extends: cc.Component,

    properties: {
        edCode:{
            default: null,
            type: cc.EditBox
        },

        labTel:{
            default:null,
            type:cc.Label
        },

        labDec:{
            default:null,
            type:cc.Label
        },

        modifyPhoneNumNewPrefab: {
            default: null,
            type: cc.Prefab
        },

        intervalTime: 60,
        _bgetVerity:true,
        _tel:true,
        _verify:"",
    },

    // use this for initialization
    onLoad: function () {
        this._tel = User.getTel();
        if(this._tel != "")
        {
            var top = "*******";
            var down = this._tel.substring(7,11);
            this.labTel.string = top+down;
        }
        this._verify = "";
    },

    _updateVerify: function(){
        this.intervalTime = this.intervalTime-1;
        this.labDec.string = this.intervalTime+"秒后重发";
        if(this.intervalTime <= 0){
            this.intervalTime = 60;
            this._bgetVerity = true;
            this.unschedule(this._updateVerify);
            this.labDec.string = "获取验证码"
        }
    },

    onTextChangedVerify:function(text, editbox, customEventData){
        var txt = text;
        if(Utils.isInt(txt) || txt == "")
            this._verify = txt;
        else
        {
            txt = this._verify;
            editbox.string = txt;
            return;
        }
     },
    
    //获取验证码
    onGetVerifyCode: function(){
        if(!this._bgetVerity)
            return;
  
        var self = this;
        var recv = function(ret){
            ComponentsUtils.unblock();
            if (ret.Code !== 0) {
                console.log(ret.Msg);
                ComponentsUtils.showTips(ret.Msg);
            }else{
                self._bgetVerity = false;
                ComponentsUtils.showTips("发送成功!");   
                self.schedule(self._updateVerify, 1);
            }
        }.bind(this);
        cc.log("发送token"+User.getLoginToken());
        var data = {
            Equipment: Utils.getEquipment(),
            Token: User.getLoginToken(),
            Mobile: this._tel,
            VerifyType:DEFINE.SHORT_MESSAGE.CHANGEBINDPHONE,
        };
        CL.HTTP.sendRequest(DEFINE.HTTP_MESSAGE.GETVERIFY, data, recv.bind(this),"POST");  
        ComponentsUtils.block();
    },

    checkInput: function(){
        if(this.edCode.string != "")
        {
            if(!Utils.isVerify(this.edCode.string))
            {
                ComponentsUtils.showTips("验证码格式不对！");
                return false;
            }
        }
        else
        {
            ComponentsUtils.showTips("验证码不能为空");
            return false;
        }
        return true;
    },

    onNext: function(){

        if(this.checkInput())
        {
            var recv = function(ret){
                ComponentsUtils.unblock();
               if (ret.Code !== 0) {
                   console.log(ret.Msg);
                   ComponentsUtils.showTips(ret.Msg);
               }else{    
                    var canvas = cc.find("Canvas");
                    this._modifyPhoneNumNewPrefab = cc.instantiate(this.modifyPhoneNumNewPrefab);
                    canvas.addChild(this._modifyPhoneNumNewPrefab);
                    this._modifyPhoneNumNewPrefab.active = true;
                    this.onClose();
               }
            }.bind(this);

            var data = {
                Token: User.getLoginToken(),
                Mobile: this._tel,
                VerifyCode: this.edCode.string,
                VerifyType:DEFINE.SHORT_MESSAGE.CHANGEBINDPHONE,
            };
            CL.HTTP.sendRequestRet(DEFINE.HTTP_MESSAGE.CHECKVERIFY, data, recv.bind(this),"POST");  
            ComponentsUtils.block();
        }
    },

    resetRefresh:function(){
        this.edCode.string = "";
        this._bgetVerity = true;
        this.unschedule(this._updateVerify);
    },

    onClose:function(){
         this.node.getComponent("Page").backAndRemove();
    }
    
});
