/*
* 追号详情界面
*/ 
cc.Class({
    extends: cc.Component,

    properties: {
        spLotteryIcon:{
             default: null,
            type: cc.Sprite
        },

        //订单金额
        labBetAmount:{
            default: null,
            type: cc.Label
        },

        //累计金额
        labRewardAmount:{
            default: null,
            type: cc.Label
        },

        //追号期数
        labFollow:{
            default: null,
            type: cc.Label
        },

        //订单状态
        labOrderStatus:{
            default: null,
            type: cc.Label
        },

        //停止条件
        labStopInfo:{
            default: null,
            type: cc.Label
        },

        //详情
        followOrderContent:{
            default:null,
            type:cc.Layout
        },

         //中奖显示
        ndRewardIcon:{
            default:null,
            type:cc.Label
        },

            //订单详情
        userOrderDetil: {
            default: null,
            type: cc.Prefab
        },

        orderTopItem:{
            default: null,
            type: cc.Prefab
        },

        orderItem:{
            default: null,
            type: cc.Prefab
        },

        _data:null,
        orderCode:0
    },

    // use this for initialization
    onLoad: function () {
        this.spLotteryIcon.spriteFrame = CL.MANAGECENTER.getLotteryHotById(this._data.LotteryCode);
        this.labBetAmount.string = LotteryUtils.moneytoServer(this._data.Amount).toString() + "元";
        this.labRewardAmount.string = LotteryUtils.moneytoServer(this._data.WinMoney).toString()+ "元";;
        this.labFollow.string = "追号共" + this._data.IsuseCount.toString() + "期";
        if(this._data.OrderStatus == 19)
        {
            this.labOrderStatus.string = "追号中";
        }
        else if(this._data.OrderStatus == 20)
        {
            this.labOrderStatus.string = "追号完成";
        }
        if(this._data.Stops == -1)
        {
            this.labStopInfo.string = "追号到截止";
        }
        else if(this._data.Stops == 0)
        {
            this.labStopInfo.string = "中奖停止追号";
        }
        else if(this._data.Stops > 0)
        {
            this.labStopInfo.string = "累计中奖金额停止";
        }
        //已追期数
        if(this._data.Already>0)
        {
            var orderTopItem = cc.instantiate(this.orderTopItem);
            var decTemp = "已追 " + this._data.Already.toString() +" 期";
            orderTopItem.getComponent('user_followOrderTop_item').init(decTemp);
            this.followOrderContent.node.addChild(orderTopItem);

            var obj = this._data.AlreadyData; 
            for(var i = 0;i<obj.length;i++)
            {
                var orderItem = cc.instantiate(this.orderItem); 
                orderItem.getComponent('user_followOrder_item').init({
                    State:obj[i].ChaseStatus,//追号状态
                    IsuseNum:obj[i].IsuseNum,//期号
                    Amount:LotteryUtils.moneytoServer(obj[i].Amount),//当期追号金额
                    WinMoney:LotteryUtils.moneytoServer(obj[i].WinMoney),//当期追号中奖金额
                    isArrow:true
                });
                var clickEventHandler = new cc.Component.EventHandler();
                clickEventHandler.target = this.node; 
                clickEventHandler.component = "user_followOrder_detail_pop";
                clickEventHandler.handler = "onClickCallBack";
                clickEventHandler.customEventData = obj[i].ChaseDetailCode;
                orderItem.getComponent(cc.Button).clickEvents.push(clickEventHandler);
                this.followOrderContent.node.addChild(orderItem);
            }
        }
        //剩余期数
        if(this._data.Surplus>0)
        {
            var orderTopItem = cc.instantiate(this.orderTopItem);
            var decTemp = "剩余 " + this._data.Surplus.toString() +" 期";
            orderTopItem.getComponent('user_followOrderTop_item').init(decTemp);
            this.followOrderContent.node.addChild(orderTopItem);

            var obj = this._data.SurplusData ; 
            for(var i = 0;i<obj.length;i++)
            {
                var orderItem = cc.instantiate(this.orderItem); 
                orderItem.getComponent('user_followOrder_item').init({
                    State:obj[i].ChaseStatus,//追号状态
                    IsuseNum:obj[i].IsuseNum,//期号
                    Amount:LotteryUtils.moneytoServer(obj[i].Amount),//当期追号金额
                    WinMoney:0,//当期追号中奖金额
                    isArrow:false
                });
                this.followOrderContent.node.addChild(orderItem);
            }
        }    

    },

    onClickCallBack:function(event, customEventData){
        var recv = function recv(ret) { 
            if (ret.Code !== 0) {
                console.log(ret.Msg);
                ComponentsUtils.showTips(ret.Msg);
            }else{  
                ComponentsUtils.unblock();
                var canvas = cc.find("Canvas");
                var userOrderDetil = cc.instantiate(this.userOrderDetil);
                userOrderDetil.getComponent('user_order_detail_pop').init(ret.Data,this._data.IsuseCount,this.orderCode,customEventData);
                canvas.addChild(userOrderDetil);
            }
        }.bind(this);
        var data = {
            Token:User.getLoginToken(),
            UserCode:User.getUserCode(),
            ChaseDetailCode:customEventData
        };
        CL.HTTP.sendRequestRet(DEFINE.HTTP_MESSAGE.GETADDTODETAILS, data, recv.bind(this),"POST");    
        ComponentsUtils.block();
    },

    init:function(data,orderCode){
        this._data = data;
        this.orderCode = orderCode;
    },

    onClose:function(){
        this.node.getComponent("Page").backAndRemove();
    },

});
